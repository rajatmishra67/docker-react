# FROM node:alpine

# WORKDIR '/app'
# COPY package.json ./
# RUN npm install
# #RUN apk add zip 
# #RUN rm -rf node_modules
# COPY ./ ./
# RUN npm run build
# RUN ls
# #CMD ["npm", "run", "dev"]
# #RUN zip -r docker-react.zip .
#  FROM nginx
#  EXPOSE 8080
#  RUN ls
#  COPY --from=0 /app/build /usr/share/nginx/html


FROM node:alpine
WORKDIR '/app'
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx
EXPOSE 80
COPY --from=0 /app/build /usr/share/nginx/html


# Clear out the node_modules and create the zip*

#RUN zip -r /app.zip /app
#RUN ls